# Kubernetes Training

`minikube` is local Kubernetes, focusing on making it easy to learn and develop for Kubernetes.

All you need is Docker (or similarly compatible) container or a Virtual Machine environment, and Kubernetes is a single command away:

    minikube start

You can use the minikube dashboard with this command, which is very useful to visualize Kubernetes!

    minikube dashboard

## Commands Overview

### Main kubectl Commands

We can use kubectl to see anything in the cluster

Get the status of the nodes

    kubectl get nodes

Get the pods

    kubectl get pods

Get the services

    kubectl get services

To create Kubernetes components we use the `create` command.

    kubectl create -h

To create a pod we use the Deployment component, which is an abstraction that creates the pods underneath
Usage: `kubectl create deployment NAME --image=image [--dry-run] [OPTIONS]`

Download latest image of Nginx from Dockerhub and create deployment

    kubectl create deployment nginx-deploy --image=nginx

This is the most minimalistic configuration for deployment: name and image. The rest is just defaults.

    kubectl get deployment

`nginx-deploy` status says it's not ready yet

    kubectl get pod

The pod `nginx-deploy-6d6565499c-jnp8r` has a prefix of the deployment + some random hash.
Status says `ContainerCreating` or `Running`.

    kubectl get replicaset

First part of the name hash is used in the name of the pod (that's how the name is made up!).
Replicaset is managing the replicas of a Pod. You in practice will never have to create/delete a Replicaset.
You'll work with deployments directly because is more convenient: you can configure the Pod blueprint completely, how many replicas, etc.

We can just edit a Deployment directly (not in the Pod) and Kubernetes manages everything else for us

    kubectl edit deployment nginx-deploy

Eg. We can change the image to `nginx:1.16` and the replicas to `2`
Vim: `Esc` and `:wq` (or `:q!` for not saving).

    kubectl get pod

We see that one pod is Terminating and another one started (Creating/Running)

    kubectl get pod

The old pod is gone and just the last one is left

    kubectl get replicaset

The old one has no Pods in it, the new one has one Pod

So we just edited the Deployment configuration and everything below that got automatically updated.

To get rid of all the Pods we have to delete the Deployment

    kubectl delete deployment nginx-deploy

So if we check the Pods, some would be terminating

    kubectl get pod

### Debugging Commands

We can see what was logged by applications running inside a Pod
Usage: `kubectl logs [pod name]`

    kubectl logs nginx-deploy-6d6565499c-k5xhq

We can get aditional information about the pods
Usage: `kubectl describe pod [pod name]`

    kubectl describe pod nginx-deploy-6d6565499c-k5xhq

We can see the state changes of the pods among other things

To get the terminal of the application inside a Pod

    kubectl exec -it nginx-deploy-6d6565499c-k5xhq -- bin/bash

### Applying Configuration File

It would be impractical to add all the configuration variables via command line

    kubectl create deployment name image option1 option2...

In practice you always work with a (blueprint) configuration file, and then tell Kubernetes to execute this file with the `apply -f` command

    kubectl apply -f simple-deployment.yaml

    kubectl get deployment

We can edit the fly and appy that again to change the deployment. Eg. we edit to `2` replicas.

    kubectl apply -f simple-deployment.yaml

You can see the `deployment configured` message, instead of the previous `deployment created` one when it didn't exist

    kubectl get pod

We can see that one Pod is younger that the other

## Demo Project

We are going to deploy two applications: MongoDB and mongo-express. Demonstrates very well a simple setup of a web application ands its database.

- MongoDB Deployment
- Internal Service: no external request, only components in the same cluster can talk to it
- Secret for the credentials

- MongoExpress Deployment
- ConfigMap for the url of the database as env. variable
- External Service (LoadBalancer): to be accessible via browser

### MongoDB Deployment & Service & Secret

We first set up a simple Deployment configuration file

To create secret

    kubectl apply -f mongodb-secret.yaml

    kubectl get secret mongodb-secret

Now we can refrence the secret keys in our configuration file

Then we create our configuration file

    kubectl apply -f mongodb-deployment.yaml

    kubectl get all

    kubectl get pod --watch

We also have the Service component to expose the MongoDB pod (config below the Deployment one)

    kubectl get service

To get all the components and filter by name

    kubectl get all | grep mongodb -> very useful!

### Mongo Express Deployment & Service & ConfigMap

We could just add the ME_CONFIG_MONGODB_SERVER value in the Deployment configuration
But we are going to use external configuration (ConfigMap) to centralize the env. variables.
This way other components would be able to use them

We create the ConfigMap which will include the server address

    kubectl apply -f mongodb-configmap.yaml

    kubectl apply -f mongo-express-deployment.yaml

    kubectl get pod

    kubectl logs mongo-express-859f75dd4f-5pbrn

Then we need a external service to access Mongo Express from a browser

    kubectl apply -f mongo-express-deployment.yaml

    kubectl get service

ClusterIP is the default type of a Service

In minikube we see that the Internal IP address is `<pending>`, since it works differently
In a regular cluster you would see there an IP address
In a LoadBalancer you have two pods binded instead of just one

In minikube, the way to assign a external service to a public IP address is:

    minikube service mongo-express-service

A browser window will open and I'll see my Mongo Express page

## Credits

This training is largely based on [Nana Janashia`s Kubernetes Tutorial for Beginners](https://www.youtube.com/watch?v=X48VuDVv0do&t=10115s&ab_channel=TechWorldwithNana), which I highly recommend!
